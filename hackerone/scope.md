| Type | Identifier | Max. Severity | In Scope |
|------|------------|---------------|----------|
| Domain | `www.jamieweb.net` | Critical | Yes |
| Domain | `jamieweb.net` | Critical | Yes |
| Domain | `ldn01.jamieweb.net` | Critical | Yes |
| Domain | `nyc01.jamieweb.net` | Critical | Yes |
| Domain | `ipv6.jamieweb.net` | Critical | Yes |
| Domain | `ipv4.jamieweb.net` | Critical | Yes |
| Domain | `acme-validation.jamieweb.net` | Critical | Yes |
| Domain | `jamiewebgbelqfno.onion` | Critical | Yes |
| Domain | `jamie3vkiwibfiwucd6vxijskbhpjdyajmzeor4mc4i7yopvpo4p7cyd.onion` | Critical | Yes |
| Source code | `https://gitlab.com/jamieweb/jamieweb` | Critical | Yes |
| Source code | `https://gitlab.com/jamieweb/jw-config` | Critical | Yes |
| Source code | `https://gitlab.com/jamieweb/web-server-log-anonymizer-bloom-filter` | Critical | Yes |
| Source code | `https://gitlab.com/jamieweb/results-whitelist` | Critical | Yes |
| Source code | `https://gitlab.com/jamieweb/dl-integrity-verify` | Critical | Yes |
| CIDR | `139.162.222.67` | Critical | Yes |
| CIDR | `2a01:7e00::f03c:91ff:fec6:27a3` | Critical | Yes |
| CIDR | `157.230.83.95` | Critical | Yes |
| CIDR | `2604:a880:400:d1::aad:8001` | Critical | Yes |
| Domain | `status.jamieweb.net` | N/A | No |
| Other | 3rd Party Mail Servers in my MX Records | N/A | No |
